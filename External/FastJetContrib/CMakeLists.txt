# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Package building the FastJet contrib libraries as part of the offline
# software build.
#

# Set the package name:
atlas_subdir( FastJetContrib )

# Set the package's dependencies:
atlas_depends_on_subdirs( PUBLIC External/FastJet )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/FastJetContribBuild )

# Set up the script used for configuring the build of FastJetConntrib.
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh @ONLY )

# Set up the build of FastJetContrib for the build area:
ExternalProject_Add( FastJetContrib
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://cern.ch/atlas-software-dist-eos/externals/FastJetContrib/fjcontrib-1.041.tar.gz
   URL_MD5 b37674a8701af52b58ebced94a270877
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E env
   CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   BUILD_IN_SOURCE 1
   BUILD_COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh make
   COMMAND
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh make install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( FastJetContrib forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of FastJetContrib..."
   DEPENDERS download )
add_dependencies( FastJetContrib FastJet )
add_dependencies( Package_FastJetContrib FastJetContrib )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/FindFastJetContrib.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )
