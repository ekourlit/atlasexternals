GeoModel I/O Library
=========================

This package builds the GeoModel I/O library for the offline software of ATLAS.

The library's sources are taken from https://gitlab.cern.ch/GeoModelDev/GeoModelIO
